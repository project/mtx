<?php
/**
 * @file
 * Installation file.
 */

/**
 * Implements hook_schema().
 */
function mtx_schema() {
  $schema = array(
    'mtx_variable' => array(
      'description' => 'Table for storing variables for different environments.',
      'fields' => array(
        'xid' => array(
          'description' => 'The unique identifier for this variable.',
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'name' => array(
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => 128,
        ),
        'value' => array(
          'type' => 'blob',
          'not null' => TRUE,
        ),
        'environment' => array(
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
        ),
      ),
      'primary key' => array('xid'),
      'unique keys' => array('variable' => array('name', 'environment')),
    ),
    'mtx_environment' => array(
      'description' => 'Table to storage environments collection',
      'fields' => array(
        'eid' => array(
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => 128,
        ),
        'label' => array(
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => 128,
        ),
      ),
      'primary key' => array('eid'),
    ),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function mtx_install() {
  if (module_load_include('inc', 'mtx', 'mtx.config')) {
    if (function_exists('_mtx_default_environment_startup')) {
      $environment_list = _mtx_default_environment_startup();
      module_load_include('inc', 'mtx', 'mtx.environments');
      _mtx_set_environment_settings($environment_list);
    }
    if (function_exists('_mtx_default_variable_startup')) {
      $variable_list = _mtx_default_variable_startup();
      module_load_include('inc', 'mtx', 'mtx.variables');
      _set_variable_settings($variable_list);
    }
    if (function_exists('_mtx_default_state_startup')) {
      $state = _mtx_default_state_startup();
      variable_set('mtx_environment', $state);
    }
  }
  if (isset($environment_list) && isset($variable_list)) {
    _mtx_set_variables_value();
  }
}


/**
 * Implements hook_uninstall().
 */
function mtx_uninstall() {
  variable_del('mtx_environment');
}
