<?php
/**
 * @file
 * Variables administration page.
 */

/**
 * Set Variable List.
 */
function mtx_variables_form($form_state) {
  $form = array();
  $current_variables_list = variable_get('mtx_variables_list');
  $form['mtx_variables_list'] = array(
    '#type' => 'textarea',
    '#title' => t('Variables List'),
    '#description' => t('Set variables list with Module|Variables'),
    '#default_value' => "$current_variables_list",
  );

  return system_settings_form($form);
}

/**
 * Get Variable List.
 */
function _mtx_get_variables_list() {
  $variables_list = array();
  $current_variables_list = variable_get('mtx_variables_list');
  $variables = explode("\n", $current_variables_list);
  foreach ($variables as $key => $variable) {
    $variable_values = explode("|", $variable);
    $variables_list[$variable_values[0]][] = trim($variable_values[1]);
  }
  return $variables_list;
}

/**
 * Recive a Variable Array and set them on variable list settings.
 */
function _set_variable_settings($variable_list) {
  $value = '';
  foreach ($variable_list as $module => $variable_list) {
    foreach ($variable_list as $variable) {
      $value .= $module . '|' . $variable . "\n";
    }
  }
  variable_set('mtx_variables_list', rtrim($value));
}

/**
 * Converts a "a|b|c" string into an array.
 */
function _mtx_parser_variable_text_into_array($variables = NULL) {
  if ($variables) {
    $items = explode("\n", $variables);
    $variable_list = array();
    foreach ($items as $item) {
      $module_variable = explode("|", $item);
      if ($module_variable[0] && $module_variable[1]) {
        if (!isset($variable_list[$module_variable[0]])) {
          $variable_list[$module_variable[0]] = array();
        }
        $variable_list[$module_variable[0]][] = $module_variable[1];
      }
      else {
        return FALSE;
      }
    }
    return $variable_list;
  }
  return FALSE;
}
