<?php
/**
 * @file
 * MTX Drush commands implementation.
 */

/**
 * Implements hook_drush_command().
 */
function mtx_drush_command() {
  $items = array();

  $items['move-to'] = array(
    'description' => "Change variables to the select environment",
    'arguments' => array(
      'state' => 'The environment [dev|test|stage|prod]',
    ),
    'examples' => array(
      'drush move-to stage' => 'Change all the variables to the ones defined for `stage` environment.',
      'drush mtx prod' => 'Change all the variables to the ones defined for `prod` environment.',
    ),
    'aliases' => array('mtx'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  return $items;
}

/**
 * Executes MTX Drush command.
 */
function drush_mtx_move_to($state) {
  if (!isset($state)) {
    return drush_set_error('Must set a environment. I.E.: move-to dev');
  }
  module_load_include('inc', 'mtx', 'mtx.environments');
  if (!_mtx_get_environment_by_id($state)) {
    $environments = implode(',', _mtx_get_environment_eid_list());
    $message = 'The environment must be lower case and be defined. The defined ones are: ' . $environments . '.';
    return drush_set_error($message);
  }
  mtx_change_state($state);
  drush_log(dt('You are now inside a new environment: !state', array('!state' => $state)), 'success');
}
