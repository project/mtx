
-- SUMMARY --

The MTX module help you to manage your project's states making easier to move
your site from, for example, development to production environments.

MTX stands for "Move To X" being "X" a site's state.

The module provides an administration form to have different variables' values
for each site state. Additionally you can manage the different states
available.

Also the module exposes a hook (hook_mtx_change_state) to allow you to execute
code during any state transition, which makes the module to fit any
requirement.

Another hook (hook_mtx_variables) is provided to allow module creators to 
include more variables to the list of variables to make multi-state.

For a full description of the module, visit the project page:
  http://drupal.org/project/mtx

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/mtx


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:

  - MTX module administration

    Perform administration tasks for MTX module.

* Configure your site's states in Administration » Configuration » MTX.


-- CONTACT --

Current maintainers:
* Norberto Bezi (ncbezi) - http://drupal.org/user/1980504


This project has been sponsored by:
* CI&T
At Ci&T, we range from building a private set of applications to engineering an
entire IT portfolio. Services include the entire application development cycle,
including service and project management, business analysis, defining
requirements, design, code, testing, implementation and support.


