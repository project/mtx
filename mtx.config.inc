<?php
/**
 * @file
 * Initial configuration file.
 */

/**
 * Return a default environment list with key id and label.
 *
 * @return array
 *   List of default environments
 */
function _mtx_default_environment_startup() {
  return array(
    'MTX_DEV' => 'Development',
    'MTX_TEST' => 'Test',
    'MTX_STAGE' => 'Stage',
    'MTX_PROD' => 'Production',
  );
}

/**
 * Return a default variable list with module name and variable array.
 *
 * @return array
 *   List of default tracked variables
 */
function _mtx_default_variable_startup() {
  return array(
    'system' => array('site_name', 'site_mail'),
  );
}

/**
 * Return a default variable list with module name and variable array.
 *
 * @return string
 *   Default current environment after installation
 */
function _mtx_default_state_startup() {
  return 'MTX_DEV';
}
