<?php
/**
 * @file
 * Administration pages
 */

/**
 * The hook_menu page callback for administration pages.
 */
function mtx_admin_configuration() {
  return drupal_get_form('mtx_admin_configuration_form');
}

/**
 * Implements hook_FORMID_form().
 */
function mtx_admin_configuration_form($form_state) {
  $form = array();
  module_load_include('inc', 'mtx', 'mtx.environments');
  $environment_id = variable_get('mtx_environment');
  $environment = _mtx_get_environment_by_id($environment_id);
  $environment_label = $environment['label'];
  $form['current_environment'] = array(
    '#type' => 'markup',
    '#markup' => t('Current environment: @environment', array('@environment' => $environment_label)),
  );

  $variables_collection = mtx_get_variables_to_mantain();
  foreach ($variables_collection as $module => $variables) {
    if (module_exists($module)) {
      foreach ($variables as $variable) {
        mtx_admin_configuration_field($form, $module, $variable, ucfirst(str_replace('_', ' ', $variable)));
      }
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#title' => t('Save'),
    '#description' => t('Save environments configuration'),
    '#submit' => array('mtx_admin_configuration_form_submit'),
  );

  $form['#submit'] = array('mtx_admin_configuration_form_submit');

  return $form;
}

/**
 * Add a variable field to the variables form.
 */
function mtx_admin_configuration_field(&$form, $module, $field_name, $title) {
  if (!isset($form[$module . '_container'])) {
    $form[$module . '_container'] = array(
      '#type' => 'fieldset',
      '#title' => $module,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
  }
  $form_container = array(
    '#type' => 'fieldset',
    '#title' => t($title . ' Values'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  module_load_include('inc', 'mtx', 'mtx.environments');
  $environment_list = _mtx_get_environment_list();
  foreach ($environment_list as $environment) {
    if ($environment['eid'] == variable_get('mtx_environment')) {
      $value = variable_get($field_name);
    }
    else {
      $value = mtx_variable_get($environment['eid'], $field_name);
    }
    $field = array(
      '#field_prefix' => '[' . $environment['label'] . ']: ',
      '#value' => $value,
      '#type' => 'textfield',
    );
    $field_name_environment = 'mtx-' . $field_name . '-env-' . $environment['eid'];
    $form_container[$field_name_environment] = $field;
  }
  $form[$module . '_container'][$field_name . '_container'] = $form_container;

}

/**
 * Submit function for variables form.
 */
function mtx_admin_configuration_form_submit($form, &$form_state) {
  foreach ($form_state['input'] as $field => $value) {
    $matches = array();
    if (preg_match('/^mtx-(.*)-env-(.*)$/', $field, $matches)) {
      mtx_variable_set($matches[2], $matches[1], $value);
    }
  }
  drupal_set_message(t('Environment configuration updated'));
}
