<?php
/**
 * @file
 * Classes for simple test plan
 */


class MtxSetEnvironmentDataBaseTestCase extends DrupalWebTestCase {

  /**
   * Implementation fo getInfo().
   */
  public static function getInfo() {
    return array(
      'name' => 'Set Environment On Data Base',
      'description' => 'Test that all values on Textarea Field are saved on Database',
      'group' => 'MTX',
    );
  }

  /**
   * Implementation fo setUp().
   */
  public function setUp() {
    return parent::setUp('mtx');
  }

  /**
   * Implementation fo testParseEnvironmentText().
   */
  public function testParseEnvironmentText() {
    $result = _mtx_parser_environment_text_into_array();
    $message = 'A NULL value should return FALSE.';
    $this->assertFalse($result, $message);

    $result = _mtx_parser_environment_text_into_array('');
    $message = 'An empty string should return FALSE.';
    $this->assertFalse($result, $message);

    $result = _mtx_parser_environment_text_into_array('eid');
    $message = 'An string without pipe should return FALSE.';
    $this->assertFalse($result, $message);

    $result = _mtx_parser_environment_text_into_array('|label');
    $message = 'An string without id should return FALSE.';
    $this->assertFalse($result, $message);

    $result = _mtx_parser_environment_text_into_array('|');
    $message = 'An string without id and label should return FALSE.';
    $this->assertFalse($result, $message);

    $result = _mtx_parser_environment_text_into_array('id|');
    $message = 'An string without label should return FALSE.';
    $this->assertFalse($result, $message);

    $result = _mtx_parser_environment_text_into_array('eid|label');
    $spect = array(
      array(
        'eid' => 'eid',
        'label' => 'label',
      ),
    );
    $message = 'An string with eid and label should be return a array with eid and label as keys.';
    $this->assertEqual($result, $spect, $message);

  }

  /**
   * Implementation fo testEnvironmentPersist().
   */
  public function testEnvironmentPersist() {
    $result = _mtx_save_environment();
    $message = 'A null value should return FALSE.';
    $this->assertFalse($result, $message);

    $result = _mtx_save_environment('');
    $message = 'A empty string should return FALSE and not an ' . $result;
    $this->assertFalse($result, $message);

    $result = _mtx_save_environment('fake_string');
    $message = 'A string should return FALSE.';
    $this->assertFalse($result, $message);

    $result = _mtx_save_environment(array());
    $message = 'A empty array should return FALSE.';
    $this->assertFalse($result, $message);

    $result = _mtx_save_environment(array('no_array_value'));
    $message = 'A no array item should return FALSE.';
    $this->assertFalse($result, $message);

    $result = _mtx_save_environment(array(array()));
    $message = 'A array item empty should return FALSE.';
    $this->assertFalse($result, $message);

    $result = _mtx_save_environment(array(array("no_id" => "value", 'label' => 'value')));
    $message = 'A array without "id" key should return FALSE.';
    $this->assertFalse($result, $message);

    $result = _mtx_save_environment(array(array("no_label" => "value", 'eid' => 'value')));
    $message = 'A array without "label" key should return FALSE.';
    $this->assertFalse($result, $message);

    $result = _mtx_save_environment(array(array("label" => "value", 'eid' => '')));
    $message = 'A empty "id" value should return FALSE.';
    $this->assertFalse($result, $message);

    $result = _mtx_save_environment(array(array("label" => "", 'eid' => 'value')));
    $message = 'A empty "label" value should return FALSE.';
    $this->assertFalse($result, $message);

    _mtx_save_environment(array(array("label" => "label", 'eid' => 'eid')));

    // TODO: ADD Generic Function.
    $spect = db_select('mtx_environment', 'e')
      ->fields('e')
      ->condition('eid', 'eid')
      ->execute()
      ->fetchAssoc();
    $message = 'The "label" value is on database.';
    $this->assertEqual($spect['label'], "label", $message);
  }

  /**
   * Implementation fo testEnvironmentSubmit().
   */
  public function testEnvironmentSubmit() {
    $result = _mtx_save_environment();
    $message = 'A null value should return FALSE.';
    $this->assertFalse($result, $message);
    $list_field = array('mtx_environment_list' => 'id|label');
    $environment_list_menu = 'admin/config/system/mtx/environment';
    $submit_form = t('Save configuration');
    $this->drupalPost($environment_list_menu, $list_field, $submit_form);
    $spect = db_select('mtx_environment', 'e')
      ->fields('e')
      ->condition('eid', 'eid')
      ->execute()
      ->fetchAssoc();
    $message = 'The "label" value is on database.';
    $this->assertEqual($spect['label'], "label", $message);
  }

  /**
   * Implementation fo checkMockEnvironment().
   */
  public function checkMockEnvironment($test) {
    $spect = db_select('mtx_environment', 'e')
      ->fields('e')
      ->condition('eid', 'eid')
      ->execute()
      ->fetchAssoc();
    $message = 'The "label" value is on database.';
    return $test->assertEqual($spect['label'], "label", $message);
  }
}

class MtxSetVariableDataBaseTestCase extends DrupalWebTestCase {

  /**
   * Implementation fo getInfo().
   */
  public static function getInfo() {
    return array(
      'name' => 'Set Variable On Data Base',
      'description' => 'Test that all values on Textarea Field are saved on Database',
      'group' => 'MTX',
    );
  }

  /**
   * Implementation fo setUp().
   */
  public function setUp() {
    return parent::setUp('mtx');
  }

  /**
   * Implementation fo testParseVariableText().
   */
  public function testParseVariableText() {
    $result = _mtx_parser_variable_text_into_array();
    $message = 'A NULL value should return FALSE.';
    $this->assertFalse($result, $message);

    $result = _mtx_parser_variable_text_into_array('');
    $message = 'An empty string should return FALSE.';
    $this->assertFalse($result, $message);

    $result = _mtx_parser_variable_text_into_array('module');
    $message = 'An string without pipe should return FALSE.';
    $this->assertFalse($result, $message);

    $result = _mtx_parser_variable_text_into_array('|variable');
    $message = 'An string without modulo should return FALSE.';
    $this->assertFalse($result, $message);

    $result = _mtx_parser_variable_text_into_array('|');
    $message = 'An string without module and variable should return FALSE.';
    $this->assertFalse($result, $message);

    $result = _mtx_parser_variable_text_into_array('module|');
    $message = 'An string without variable should return FALSE.';
    $this->assertFalse($result, $message);

    $result = _mtx_parser_variable_text_into_array('module|variable');
    $spect = array(
      'module' => array('variable'),
    );
    $message = 'Array structure is valid.';
    $this->assertEqual($result, $spect);

    $result = _mtx_parser_variable_text_into_array("module1|variable1
module2|variable2
module2|variable3");
    $spect = array(
      'module1' => array('variable1'),
      'module2' => array('variable2', 'variable3'),
    );
    $message = 'Array structure is valid.';
    $this->assertEqual($result, $spect);
  }
}
