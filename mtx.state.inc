<?php
/**
 * @file
 * Change State pages.
 */

/**
 * Menu callback.
 */
function mtx_admin_change_state() {
  return drupal_get_form('mtx_change_state_form');
}

/**
 * Change state form API implementation.
 */
function mtx_change_state_form($form_state) {
  $form = array();
  $options = _mtx_get_environment_option();
  $current_state = variable_get('mtx_environment');
  $form['new_state'] = array(
    '#type' => 'radios',
    '#title' => t('New State'),
    '#options' => $options,
    '#default_value' => "$current_state",
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Change State'),
  );

  $form['#submit'] = array('mtx_change_state_form_submit');
  return $form;
}

/**
 * Handles change state form submition.
 */
function mtx_change_state_form_submit($form, &$form_state) {
  $new_state = $form_state['input']['new_state'];
  if (variable_get('mtx_environment') == $new_state) {
    drupal_set_message(t("New state can't be the current state... please"), 'error');
  }
  else {
    mtx_change_state($new_state);
    drupal_set_message(t('You are now under a new environment reality! Make sure to check your site before spread the word.'));
  }
}

/**
 * Returns all the environments options.
 */
function _mtx_get_environment_option() {
  module_load_include('inc', 'mtx', 'mtx.environments');
  $environment_list = _mtx_get_environment_list();
  $environment_option = array();
  foreach ($environment_list as $environment) {
    $environment_option[$environment['eid']] = t('Move to @label', array('@label' => $environment['label']));
  }

  return $environment_option;
}
