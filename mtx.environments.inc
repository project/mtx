<?php
/**
 * @file
 * Administration UI for system environments
 */

/**
 * Set Environment List variable.
 */
function mtx_environments_form($form_state) {
  $form = array();

  $current_environment_list = _mtx_serialize_enviroments_list();
  $form['mtx_environment_list'] = array(
    '#type' => 'textarea',
    '#title' => t('Environment List'),
    '#description' => t('Set environment list with Id|Label'),
    '#default_value' => "$current_environment_list",
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['#submit'][] = 'mtx_environments_form_submit';

  return $form;
}

/**
 * Submit function for environments form.
 */
function mtx_environments_form_submit($form_state) {
  $environment_value = $form_state['mtx_environment_list']['#value'];
  $environment_list = _mtx_parser_environment_text_into_array($environment_value);
  if ($environment_list) {
    if (_mtx_save_environment($environment_list)) {
      $message = 'Environment was saved';
      drupal_set_message(check_plain($message));
    }
    else {
      form_set_error('mtx_environment_validate', 'Error saving environment');
    }
  }
}

/**
 * Input environment texarea field value and return array list.
 */
function _mtx_parser_environment_text_into_array($variables = NULL) {
  if ($variables) {
    $environments = explode("\n", $variables);
    $environment_list = array();
    foreach ($environments as $key => $environment) {
      $environment_values = explode("|", $environment);
      if ($environment_values[0] && isset($environment_values[1]) && $environment_values[1]) {
        $environment_list[$key]['eid'] = $environment_values[0];
        $environment_list[$key]['label'] = trim($environment_values[1]);
      }
      else {
        return FALSE;
      }
    }
    return $environment_list;
  }
  else {
    return FALSE;
  }
}

/**
 * Returns the environment list.
 */
function _mtx_get_environment_list() {
  $resource = db_select('mtx_environment', 'e')
    ->fields('e')
    ->execute();
  $enviroments = array();
  foreach ($resource as $key => $enviroment) {
    $enviroments[$key]['eid'] = $enviroment->eid;
    $enviroments[$key]['label'] = $enviroment->label;
  }

  return $enviroments;
}

/**
 * Prepares the environments list to be saved in the database.
 */
function _mtx_serialize_enviroments_list() {
  $environment_list = _mtx_get_environment_list();
  foreach ($environment_list as $environment) {
    $environments[] = implode('|', $environment);
  }
  return (implode("\n", $environments));
}

/**
 * Get environment by ID.
 */
function _mtx_get_environment_by_id($environment_id) {
  $environment_list = _mtx_get_environment_list();
  foreach ($environment_list as $environment) {
    if ($environment['eid'] == $environment_id) {
      return $environment;
    }
  }

  return FALSE;
}

/**
 * Receive a Environment Array and set them on environment list settings.
 */
function _mtx_set_environment_settings($environment_list) {
  $environments = array();
  foreach ($environment_list as $environment => $label) {
    $environments[] = array(
      'eid' => $environment,
      'label' => $label,
    );
  }
  _mtx_save_environment($environments);
}

/**
 * Saves environments list to the database.
 */
function _mtx_save_environment(array $environments) {
  if (count($environments) > 0) {
    foreach ($environments as $environment) {
      if (is_array($environment) && count($environment) > 0) {
        if ($environment['eid'] && $environment['label']) {
          // TODO: Delete environment.
          $result = db_merge('mtx_environment')
                      ->key(array('eid' => $environment['eid']))
                      ->fields(array('label' => $environment['label']))
                      ->execute();
          // TODO: Use Constant Value.
          if ($result == 1) {
            _mtx_set_variables_value(array($environment));
          }
          if (!$result) {
            return FALSE;
          }
        }
        else {
          return FALSE;
        }
      }
      else {
        return FALSE;
      }
    }
    return TRUE;
  }
  return FALSE;
}

/**
 * Returns the environment description list.
 */
function _mtx_get_environment_description_list() {
  $enviromnets = _mtx_get_environment_list();
  $enviromnets_description = array();
  foreach ($enviromnets as $enviromnet) {
    $enviromnets_description[] = $enviromnet['label'];
  }
  return $enviromnets_description;
}

/**
 * Returns the environment description list.
 */
function _mtx_get_environment_eid_list() {
  $enviromnets = _mtx_get_environment_list();
  $enviromnets_description = array();
  foreach ($enviromnets as $enviromnet) {
    $enviromnets_description[] = $enviromnet['eid'];
  }
  return $enviromnets_description;
}
